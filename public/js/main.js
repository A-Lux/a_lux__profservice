$(document).ready(function () {
    AOS.init();
    $('.btn-anchor').click(function (e) {
        e.preventDefault();
        var anchor = $(this).attr('href'),
            top = $(anchor).offset().top;

        $('body,html').animate({ scrollTop: top }, 1000);
    });


    $('.mobile-menu .burger-menu-btn').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('burger-menu-lines-active');
        $('.mobile-nav').toggleClass('mobile-nav-active');
    });

    let projectSlider = $('.project-slider');
    projectSlider.owlCarousel({
        loop: true,
        dots: true,
        arrows: false,
        autoplay: true,
        margin: 0,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    function owlDotsWrap(el) {
        let owldots = el;
        let container = document.createElement("div");
        container.classList.add("container");
        let row = document.createElement("div");
        // row.classList.add("row");
        let col = document.createElement("div");
        col.classList.add("col-12");
        owldots.parentNode.insertBefore(col, owldots);
        col.appendChild(owldots);
        col.parentNode.insertBefore(row, col);
        row.appendChild(col);
        row.parentNode.insertBefore(container, row);
        container.appendChild(row);
    };
    owlDotsWrap(document.getElementsByClassName("owl-dots")[0]);

    // let owlDots = document.createElement('div');
    // owlDots.innerHTML = "<div class='owl - dots'><button role='button' class='owl - dot'><span></span></button><button role='button' class='owl - dot'><span></span></button><button role='button' class='owl - dot active'><span></span></button></div>"
    // console.log(owlDots);
    // $('.main-text').append("<div class='owl - dots'><button role='button' class='owl - dot'><span></span></button><button role='button' class='owl - dot'><span></span></button><button role='button' class='owl - dot active'><span></span></button></div>")

    let projectNameSlider = $('.project-name-slider');
    projectNameSlider.owlCarousel({
        loop: true,
        dots: false,
        arrows: false,
        margin: 0,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('.arrow-left').click(function (e) {
        e.preventDefault();
        projectNameSlider.trigger('prev.owl.carousel');
    });
    $('.arrow-right').click(function (e) {
        e.preventDefault();
        projectNameSlider.trigger('next.owl.carousel');
    })

    let clientsSlider = $('.clients-slider');
    clientsSlider.owlCarousel({
        loop: false,
        dots: false,
        arrows: false,
        margin: 30,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 4
            }
        }
    });
    $('.clients-left').click(function (e) {
        e.preventDefault();
        clientsSlider.trigger('prev.owl.carousel');
    });
    $('.clients-right').click(function (e) {
        e.preventDefault();
        clientsSlider.trigger('next.owl.carousel');
    });

    let vacanciesSlider = $('.vacancies-slider');
    vacanciesSlider.owlCarousel({
        loop: true,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplayTimeout: 4000,
        margin: 30,
        center: true,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
    $('.vacancies-left').click(function (e) {
        e.preventDefault();
        vacanciesSlider.trigger('prev.owl.carousel');
    });

    $('.vacancies-right').click(function (e) {
        e.preventDefault();
        vacanciesSlider.trigger('next.owl.carousel');
    })
    $('.textClick').click(function () {
        console.log('afkafo');
        if (this.innerHTML == "Свернуть") {
            this.innerHTML = "...";
        } else {
            this.innerHTML = "Свернуть";
        }
    });
    $('.btn-order').mouseenter(function () {
        $(this).parent().addClass('border-card');
    });
    $('.btn-order').mouseleave(function () {
        $('.services-card').removeClass('border-card');
    });
});
