$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function showOverflow() {
    $("body").css("overflow-y","visible");
}

function showSuccessAlert(text){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true
    });
    Toast.fire({
        icon: 'success',
        title: text
    });
}

function saveServiceID(service_id){
    $("#requestServiceModal #request_service_id").val(service_id);
}

function saveVacancyID(vacancy_id){
    $("#requestVacancyModal #request_vacancy_id").val(vacancy_id);
}


function saveProjectID(project_id){
    $("#requestProjectModal #request_project_id").val(project_id);
}



$('body').on('click','#requestButton', function(){
    $("body").css("overflow-y","visible");
    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/site',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $(".contacts form")[0].reset();
                showSuccessAlert(response.message);
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }

        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});



$('body').on('click','#clientRequestButton', function(){
    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/client',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $('#requestClientModal').modal('hide');
                $('.modal-backdrop').css('display','none');
                $("#requestClientModal form")[0].reset();
                showSuccessAlert(response.message);
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }

        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});


$('body').on('click','#serviceRequestButton', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/service',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $('#requestServiceModal').modal('hide');
                $('.modal-backdrop').css('display','none');
                $("#requestServiceModal form")[0].reset();
                showSuccessAlert(response.message);
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }

        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});




$('body').on('click','#vacancyRequestButton', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/vacancy',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $('#requestVacancyModal').modal('hide');
                $('.modal-backdrop').css('display','none');
                $("#requestVacancyModal form")[0].reset();
                showSuccessAlert(response.message);
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }

        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});




$('body').on('click','#projectRequestButton', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/project',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $('#requestVacancyModal').modal('hide');
                $('.modal-backdrop').css('display','none');
                $("#requestProjectModal form")[0].reset();
                showSuccessAlert(response.message);
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }

        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});


