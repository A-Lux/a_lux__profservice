<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/owl.theme.default.min.css">
    <link rel="stylesheet" href="./css/owl.carousel.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>

<!-- MOBILE-HEADER -->
<div class="mobile-menu">
    <div class="logo">
        <a href="#"><img src="./images/logo.png" alt=""></a>
    </div>
    <div class="lang d-flex">
        <a href="#" class="lang-active">ru</a>
        <a href="#">kz</a>
        <a href="#">en</a>
    </div>
    <div class="burger-menu">
        <a href="#" class="burger-menu-btn">
            <span class="burger-menu-lines"></span>
        </a>
    </div>
    <div class="mobile-nav">
        <ul class="mb-0 p-0">
            <li>
                <a href="#">Главная</a>
                <a href="#">О КОМПАНИИ </a>
                <a href="#">ПРОЕКТЫ</a>
                <a href="#">УСЛУГИ </a>
                <a href="#">БРЕНДЫ</a>
                <a href="#">КОНТАКТЫ</a>
            </li>
        </ul>
    </div>
</div>
<!-- MOBILE-HEADER-END  -->

<div class="main-block">
    <div class="container">
        <div class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand" href="#"><img src="./images/logo.png" alt=""></a>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <ul class="d-flex p-0 m-0">
                        <li>
                            <p>+7 727 367-02-19</p>
                        </li>
                        <li><a class="nav-item" href="#">info@profservice.kz</a></li>
                    </ul>
                    <div class="lang d-flex">
                        <a href="#" class="lang-active">ru</a>
                        <a href="#">kz</a>
                        <a href="#">en</a>
                    </div>
                </div>
            </div>
        </div>
        <nav>
            <a href="#">ГЛАВНАЯ</a>
            <a href="#">О КОМПАНИИ</a>
            <a href="#">ПРОЕКТЫ</a>
            <a href="#">УСЛУГИ</a>
            <a href="#">БРЕНДЫ</a>
            <a href="#">КОНТАКТЫ</a>
        </nav>
    </div>
</div>
<div class="mian-slider">
    <div class="owl-carousel project-slider owl-theme">
        <div class="item">
            <img src="./images/index-slider.png" alt="">
            <div class="container">
                <div class="main-text">
                    <h1>Ваш проводник <br> в мире технологий</h1>

                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem esse aperiam quam itaque
                        debitis
                        dolorum iure assumenda, excepturi sint atque velit aliquam nulla optio? Itaque totam amet
                        dolore ad
                        saepe?</p>

                    <a href="#" type="button" class="btn btn-danger btn-service">читать подробнее</a>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="./images/index-slider.png" alt="">
        </div>
        <div class="item">
            <img src="./images/index-slider.png" alt="">
        </div>
    </div>
</div>


<!-- ABOUT-MAIN -->
<div class="about-main text-center">
    <span class="about-bg"></span>
    <div class="container">
        <div class="title">
            <h1>О компании</h1>
        </div>
        <div class="about-text">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                Cum
                sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
                ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo,
                fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet aLorem ipsum
                dolor
                sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                natoque
                penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
                pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel,
                aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a</p>
        </div>
        <div class="about-content">
            <div class="row justify-content-center">
                <div class="col-xl-5 p-0">
                    <div>
                    </div>
                </div>
                <div class="col-xl-1 line"></div>
                <div class="col-xl-5 p-0">
                    <div class="about-right">
                        <div class="date">
                            <h4>2014</h4>
                        </div>
                        <div class="right-text">
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maiores ducimus nostrum quasi
                                deleniti itaque ullam asperiores ipsum consectetur commodi rem, quisquam eos odio
                                delectus culpa reprehenderit corporis similique voluptas ipsam, minima consequatur optio
                                rerum consequuntur et. Eaque sed similique error.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 p-0">
                    <div class="about-left">
                        <div class="date">
                            <h4>2016</h4>
                        </div>
                        <div class="left-text">
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maiores ducimus nostrum quasi
                                deleniti itaque ullam asperiores ipsum consectetur commodi rem, quisquam eos odio
                                delectus culpa reprehenderit corporis similique voluptas ipsam, minima consequatur optio
                                rerum consequuntur et. Eaque sed similique error.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 line"></div>
                <div class="col-xl-5">
                    <div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div>
                    </div>
                </div>
                <div class="col-xl-1 line"></div>
                <div class="col-xl-5">
                    <div class="about-right">
                        <div class="date">
                            <h4>2018</h4>
                        </div>
                        <div class="right-text">
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maiores ducimus nostrum quasi
                                deleniti itaque ullam asperiores ipsum consectetur commodi rem, quisquam eos odio
                                delectus culpa reprehenderit corporis similique voluptas ipsam, minima consequatur optio
                                rerum consequuntur et. Eaque sed similique error.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div class="about-left">
                        <div class="date">
                            <h4>2019</h4>
                        </div>
                        <div class="right-text">
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maiores ducimus nostrum quasi
                                deleniti itaque ullam asperiores ipsum consectetur commodi rem, quisquam eos odio
                                delectus culpa reprehenderit corporis similique voluptas ipsam, minima consequatur optio
                                rerum consequuntur et. Eaque sed similique error.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 end-line"></div>
                <div class="col-xl-5">
                    <div>
                    </div>
                </div>
                <button class="btn btn-danger btn-service mt-4"><span class="mr-2"><img src="./images/email.png" alt=""></span> стать нашим клиентом!</button>
            </div>
        </div>
    </div>
</div>

<!-- ABOUT-MAIN-END -->

<!-- OUR-SERVICE -->
<div class="our-service text-center">
    <div class="container">
        <div class="title">
            <h1>Наши услуги</h1>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_1.png" alt="">
                    </div>
                    <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной печатно-множительной техники</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_2.png" alt="">
                    </div>
                    <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_3.png" alt="">
                    </div>
                    <h5>Внедрение проектов под ключ</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_4.png" alt="">
                    </div>
                    <h5>Диагностика существующей инфраструктуры ИТ</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_5.png" alt="">
                    </div>
                    <h5>ИТ и ИКТ консалтинг и разработка концепции проекта</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_6.png" alt="">
                    </div>
                    <h5>Консалтинг, разработка моделей и сервисная поддержка в области 3D печати</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_7.png" alt="">
                    </div>
                    <h5>Независимая экспертиза предлагаемых на рынке решений</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_8.png" alt="">
                    </div>
                    <h5>Проектирование инженерной инфраструктуры</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_9.png" alt="">
                    </div>
                    <h5>Проектная деятельность и строительно-монтажные работы до 3-ей категории сложности</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_10.png" alt="">
                    </div>
                    <h5>Разработка программного обеспечения</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_11.png" alt="">
                    </div>
                    <h5>Сервисная поддержка</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_12.png" alt="">
                    </div>
                    <h5>Техническое оснащение государственных мероприятий.</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_13.png" alt="">
                    </div>
                    <h5>Управление проектами цифровой трансформации</h5>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="our-card">
                    <div class="blue-img">
                        <img src="./images/ic_14.png" alt="">
                    </div>
                    <h5>Услуги по 3D-печати из различных полимерных материалов и металлического порошка</h5>
                </div>
            </div>
        </div>
        <button class="btn btn-danger btn-service">Полный список услуг</button>
    </div>
</div>

<!-- OUR-SERVICE-END -->

<!-- LAST-PROJECTS -->

<div class="last-project">
    <div class="container">
        <div class="title">
            <h1>Последние проекты</h1>
        </div>
        <div class="owl-carousel owl-theme text-center project-slider">
            <div class="item">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                    massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec
                    quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,
                    imperdiet aLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                    ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                    massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo,
                    rhoncus ut, imperdiet a</p>
            </div>
            <div class="item">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                    massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec
                    quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,
                    imperdiet aLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                    ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                    massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo,
                    rhoncus ut, imperdiet a</p>
            </div>
            <div class="item">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                    massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec
                    quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,
                    imperdiet aLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                    ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                    massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo,
                    rhoncus ut, imperdiet a</p>
            </div>
        </div>
    </div>
</div>

<!-- LAST-PROJECTS-END -->

<!-- PROJECT-NAME -->

<div class="project-name">
    <div class="arrow-left">
        <a href="#"><img src="./images/arrow-left.png" alt=""></a>
    </div>
    <div class="arrow-right">
        <a href="#"><img src="./images/arrow-right.png" alt=""></a>
    </div>
    <div class="owl-carousel owl-theme project-name-slider">
        <div class="item">
            <div class="col-xl-6 p-0 text-right">
                <div class="project-name-item">
                    <div class="container">
                        <div class="name">
                            <h2>Название проекта</h2>
                        </div>
                        <div class="project-name-icon d-flex justify-content-end">
                            <span>
                                <img src="./images/ic-design.png" alt="">
                            </span>
                            <span>
                                <img src="./images/ic-development.png" alt="">
                            </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                            Aenean
                            massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Donec
                            quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                            enim.
                            Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus
                            ut,
                            imperdiet aLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula
                            eget
                            dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                            ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
                            consequat
                            massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim
                            justo,
                            rhoncus ut, imperdiet a</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">2</div>
        <div class="item">3</div>
    </div>
</div>

<!-- PROJECT-NAME-END -->

<!-- CLIENTS -->
<div class="clients">
    <div class="container">
        <div class="title">
            <h1>Наши клиенты</h1>
        </div>
        <a href="#" class="clients-left"><span><img src="./images/left-client.png" alt=""></span></a>
        <a href="#" class="clients-right"><span><img src="./images/right-client.png" alt=""></span></a>
        <div class="owl-carousel owl-theme clients-slider">
            <div class="item">
                <div class="clients-card text-center">
                    <div class="clients-img">
                        <img src="./images/amx.png" alt="">
                    </div>
                    <div class="clients-card-text">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                            Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                            ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="clients-card text-center">
                    <div class="clients-img">
                        <img src="./images/sharp.png" alt="">
                    </div>
                    <div class="clients-card-text">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                            Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                            ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="clients-card text-center">
                    <div class="clients-img">
                        <img src="./images/bitmap.png" alt="">
                    </div>
                    <div class="clients-card-text">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                            Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                            ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="clients-card text-center">
                    <div class="clients-img">
                        <img src="./images/logicech.png" alt="">
                    </div>
                    <div class="clients-card-text">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                            Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                            ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CLIENTS-END -->


<!-- CONTACTS-END -->























<?php include('footer.php'); ?>