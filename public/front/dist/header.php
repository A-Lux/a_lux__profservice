<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/owl.theme.default.min.css">
    <link rel="stylesheet" href="./css/owl.carousel.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>

<body>
    <!-- MOBILE-HEADER -->
    <div class="mobile-menu">
            <div class="logo">
                <a href="#"><img src="./images/logo.png" alt=""></a>
            </div>
            <div class="lang d-flex">
                <a href="#" class="lang-active">ru</a>
                <a href="#">kz</a>
                <a href="#">en</a>
            </div>
            <div class="burger-menu">
                <a href="#" class="burger-menu-btn">
                    <span class="burger-menu-lines"></span>
                </a>
            </div>
            <div class="mobile-nav">
                <ul class="mb-0 p-0">
                    <li>
                        <a href="#">Главная</a>
                        <a href="#">О КОМПАНИИ </a>
                        <a href="#">ПРОЕКТЫ</a>
                        <a href="#">УСЛУГИ </a>
                        <a href="#">БРЕНДЫ</a>
                        <a href="#">КОНТАКТЫ</a>
                    </li>
                </ul>
            </div>
        </div>
    <!-- MOBILE-HEADER-END  -->
    <header>
        <div class="container">
            <div class="navbar navbar-expand-lg navbar-light p-0">
                <a class="navbar-brand" href="#"><img src="./images/logo.png" alt=""></a>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <ul class="d-flex p-0 m-0">
                            <li>
                                <p>+7 727 367-02-19</p>
                            </li>
                            <li><a class="nav-item" href="#">info@profservice.kz</a></li>
                        </ul>
                        <div class="lang d-flex">
                            <a href="#" class="lang-active">ru</a>
                            <a href="#">kz</a>
                            <a href="#">en</a>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <a href="#" class="active-index">ГЛАВНАЯ</a>
                <a href="#" class="active-link">О КОМПАНИИ</a>
                <a href="#">ПРОЕКТЫ</a>
                <a href="#">УСЛУГИ</a>
                <a href="#">БРЕНДЫ</a>
                <a href="#">КОНТАКТЫ</a>
            </nav>
        </div>
    </header>








