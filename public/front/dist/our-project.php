<?php include('header.php'); ?>

<div class="projects">
    <div class="container">
        <div class="title">
            <h1>Проекты</h1>
        </div>
        <div class="projects-discription text-center">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet aLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a</p>
        </div>
    </div>
</div>

<div class="univer-block">
    <div class="row w-100 m-0">
        <div class="col-xl-6 col-lg-6">
            <div class="container">
                <div class="univer-left-block">
                    <h2>Казахский Национальный Аграрный Университет</h2>
                    <p>Внедрение инфокоммуникационных технологий в одном из ведущих высших учебных заведений Республики Казахстан - «Казахский Национальный Аграрный Университет».</p>
                    <button class="btn btn-danger btn-service">заказать подобный проект</button>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 p-0">
            <div class="univer-right-block">
                <div class="univer-right-head">
                    <div class="univer-right-content d-flex align-items-center">
                        <img src="./images/univer-right-1.png" alt="">
                        <h5>Диагностика существующей
                            инфраструктуры ИТ</h5>
                    </div>
                    <div class="univer-right-content d-flex align-items-center">
                        <img src="./images/univer-right-2.png" alt="">
                        <h5>Проектирование инженерной
                            инфраструктуры</h5>
                    </div>
                    <div class="univer-right-content d-flex align-items-center">
                        <img src="./images/univer-right-3.png" alt="">
                        <h5>Техническое
                            оснащение </h5>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="ministr-block">
    <div class="row w-100 m-0">
        <div class="col-xl-6 col-lg-6 p-0">
            <div class="ministr-left-block">
                <div class="ministr-left-head">
                    <div class="ministr-left-content d-flex align-items-center">
                        <img src="./images/univer-right-1.png" alt="">
                        <h5>Диагностика существующей
                            инфраструктуры ИТ</h5>
                    </div>
                    <div class="ministr-left-content d-flex align-items-center">
                        <img src="./images/univer-right-2.png" alt="">
                        <h5>Проектирование инженерной
                            инфраструктуры</h5>
                    </div>
                    <div class="ministr-left-content d-flex align-items-center">
                        <img src="./images/univer-right-3.png" alt="">
                        <h5>Техническое
                            оснащение </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="container">
                <div class="ministr-right-block">
                    <h2>Казахский Национальный Аграрный Университет</h2>
                    <p>Комплексное оснащение Министерства Финансов Республики Казахстан компьютерной и печатной техникой от ведущих мировых производителей. </p>
                    <button class="btn btn-danger btn-service">заказать подобный проект</button>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('footer.php'); ?>