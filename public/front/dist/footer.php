<!-- CONTACTS -->

<div class="contacts">
    <div class="row w-100">
        <div class="col-xl-6 col-lg-6">
            <div class="map">
                <iframe src="https://yandex.kz/map-widget/v1/-/CGxgRZ6t" width="100%" height="100%" frameborder="1" allowfullscreen="true"></iframe>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="contacts-content">
                <div class="title">
                    <h1>Свяжитесь с нами</h1>
                </div>
                <form action="">
                    <label for="">Ваше имя</label>
                    <input type="text" placeholder="Сергей Федорец">
                    <br>
                    <label for="">Email</label>
                    <input type="text" placeholder="Feborec_gmail.com">
                    <br>
                    <label for="">Комментарий</label>
                    <textarea name="" id="" cols="30" rows="10" placeholder="Ваше сообщене"></textarea>
                </form>
                <button class="btn btn-danger btn-service mt-4">Отправить</button>
            </div>
        </div>
    </div>
</div>


<!-- CONTACTS-END -->

<footer>
    <div class="container">
        <div class="footer-link d-flex">
            <a href="#">Главная</a>
            <a href="#">О КОМПАНИИ </a>
            <a href="#">ПРОЕКТЫ</a>
            <a href="#">УСЛУГИ </a>
            <a href="#">БРЕНДЫ</a>
            <a href="#">БРЕНДЫ</a>
        </div>
        <div class="footer-adress">
            <div class="row">
                <div class="col-xl-6">
                    <div class="adress">
                        <p>Нур-Султан, Улица Дінмұхамед Конаев, 29/1,
                           <br> Офис 20 Этаж, Офис 2008</p>
                        <div class="silver-text">
                            <p>Консультационные и практические услуги в области информационных технологий</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 text-right d-flex justify-content-end">
                    <div class="email">
                        <a href="#">
                            <h4>+7 495 215-29-48</h4>
                        </a>
                        <a href="#">info@profservice.kz</a>
                    </div>
                    <div class="social">
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-youtube"></i></a>
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>










<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="./js/owl.carousel.min.js"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="./js/main.js"></script>
</body>

</html>