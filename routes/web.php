<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

// pages
Route::get('/', 'HomeController@index')->name('home');
Route::get('/lang/{url}', 'LangController@index');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/projects', 'ProjectsController@index')->name('projects');
Route::get('/services', 'ServicesController@index')->name('services');
Route::get('/projects', 'ProjectsController@index')->name('projects');
Route::get('/brands', 'BrandsController@index')->name('brands');
Route::get('/contacts', 'ContactsController@index')->name('contacts');


// Request
Route::post('/request/site', 'RequestController@site');
Route::post('/request/client', 'RequestController@client');
Route::post('/request/service', 'RequestController@service');
Route::post('/request/vacancy', 'RequestController@vacancy');
Route::post('/request/project', 'RequestController@project');
