@extends('layouts.app')
@section('content')

<div class="container">
    <div class="contacts-page">
        <div class="title">
            <h1>{{$model->title}}</h1>
        </div>
        <div class="contacts-content text-center col-xl-7 m-auto">
            <b> @lang('messages.ТОО «ProfService»'), </b>
            <p>
                {{$address->getAddress()}},
            </p>
            <br>
            <span>e-mail: </span><a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>
            <br>
            <button class="btn btn-danger btn-service" data-toggle="modal" data-target="#requestClientModal">
                <img src="./images/email.png" alt=""> @lang('messages.стать нашим клиентом!')
            </button>
            <div class="social-contact">
                <p>@lang('messages.Мы в социальных сетях'):</p>
                <div class="social-icon d-flex align-items-center justify-content-center">
                    <a href="{{setting('site.facebook')}}"><i class="fab fa-facebook"></i></a>
                    <a href="{{setting('site.linkedin')}}"><img src="./images/icon-linkedin.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection