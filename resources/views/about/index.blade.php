@extends('layouts.app')
@section('content')


<div class="projects-about">
    <div class="container">
        <div class="title" data-aos="zoom-in">
            <h1>{{$model->title}}</h1>
        </div>
        <div class="projects-discription about-discription text-center" data-aos="zoom-in">
            {!! $model->content !!}
        </div>
    </div>
    <div class="about-content">
        <div class="about-block">
            <div class="row w-100 m-0">
                <div class="col-xl-5 col-lg-6">
                    <div class="container">
                        <div class="about-left-block" data-aos="fade-right">
                            <h2>{{$staff->title}}</h2>
                            <p>
                                {!! $staff->content !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 p-0">
                    <div class="about-right-block" style="background-image: url({{ Voyager::image($staff->background)}});">
                        <div class="about-right-head d-flex align-items-center h-100">
                            @foreach($staffTypes as $k => $v)
                            <div class="about-right-content align-items-center text-center">
                                <img src="{{ asset('storage/'.$v->icon)}}" alt="">
                                <h5>{{$v->text}}</h5>
                            </div>
                            @endforeach

                        </div>
                        <!-- <button class="btn btn-danger btn-service mt-4" data-toggle="modal" data-target="#requestClientModal">
                            <span class="mr-2"><img src="./images/email.png" alt=""></span> @lang('messages.стать нашим клиентом!')
                        </button> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="companies-portfolio">
        <div class="container">
            <div class="title">
                <h2>{{$portfolio->title}}</h2>
            </div>
            <span class="about-bg"></span>
            <div class="portfolio-content">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="companies-logo">
                            <div class="row">
                                @php $m=0;@endphp
                                @foreach($portfolioBrands as $k => $v)
                                @php $m++;
                                if(count($portfolioBrands) == $m) $class = "col-xl-4 col-lg-3 p-0";
                                elseif($m > 8 && $m < 11) $class="col-xl-4 col-6 col-md-6 col-lg-3 p-0" ;
                                elseif($m == 6) $class="col-xl-2 col-6 col-md-6 col-lg-3 p-0" ; 
                                elseif($m == 7) $class="col-xl-4 col-6 col-md-6 col-lg-3 p-0" ; 
                                else $class="col-xl-3 col-6 col-md-6 col-lg-3 p-0" ; @endphp 
                                <div class="{{$class}}">
                                    <div class="logo" data-aos="zoom-in">
                                        <img src="{{ asset('storage/'.$v->image)}}" alt="">
                                    </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="portfolio-text">
                        <p>
                            {!! $portfolio->content !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="major-companies">
    <div class="container">
        <div class="title">
            <h1>{{$mainDirection->title}} </h1>
        </div>
        <div class="projects-discription about-discription text-center">
            <p>
                {{$mainDirection->content}}
            </p>
        </div>
        <div class="row">
            @foreach($services as $k => $v)
                    <div class="col-xl-3 col-md-6 col-lg-4" data-aos="fade-up">
                        <div class="blue-logo text-center">
                            <img src="{{ asset('storage/'.$v->image)}}" alt="">
                            <h5>{{$v->name}}</h5>
                        </div>
                    </div>
            @endforeach
        </div>

        <!-- @if(count($services) > 4)
        <div class="collapse" id="collapseExample">
            <div class="row">
                @php $m=0; @endphp
                @foreach($services as $k => $v)
                    @php $m++; @endphp
                    @if($m > 4)
                        <div class="col-xl-3 col-md-6 col-lg-4" data-aos="fade-up">
                            <div class="blue-logo text-center">
                                <img src="{{ asset('storage/'.$v->image)}}" alt="">
                                <h5>{{$v->name}}</h5>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        @endif -->
    </div>

    <!-- @if(count($services) > 4)
    <a class="btn btn-danger btn-service" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">@lang('messages.Полный список услуг')</a>
    @endif -->
</div>

<!-- @if($vacancies != null)
<div class="container">
    <div class="vacancies">
        <div class="title">
            <h1>@lang('messages.Открытые вакансии')</h1>
        </div>
        <div class="owl-carousel owl-theme vacancies-slider">
            @foreach($vacancies as $k => $v)
            <div class="item" data-aos="flip-down">
                <h5>{{$v->name}}</h5>
                <br>
                <div class="vacancies-title">
                    <p><b>@lang('messages.З/П:')</b><span class="red-text">{{$v->patch}}</span></p>
                </div>
                <div class="vacancies-first-text">
                    <p>{{$v->content}}</p>
                </div>
                @if($v->requirements != null || $v->social_package != null)
                <div class="vacancies-last-text collapse" id="collapseExample{{$v->id}}">
                    <b>@lang('messages.Требования')</b>
                    <p>{{$v->requirements}}</p>
                    <b>@lang('messages.Соцпакет')</b>
                    <p>{{$v->social_package}}</p>
                </div>
                @endif
                <div class="textHide">
                    <a class="red-text textClick" data-toggle="collapse" href="#collapseExample{{$v->id}}" aria-expanded="false" aria-controls="collapseExample">

                        @if($v->requirements != null || $v->social_package != null)
                        ...
                        @endif
                    </a>
                </div>

                <button class="btn btn-outline-danger btn-outline" data-toggle="modal" data-target="#requestVacancyModal" onclick="saveVacancyID({{$v->id}})">
                    @lang('messages.Подать заявку')</button>
            </div>
            @endforeach
        </div>
        <a href="#" class="vacancies-left">
            <span>
                <img src="./images/left-client.png" alt="">
            </span>
        </a>
        <a href="#" class="vacancies-right">
            <span>
                <img src="./images/right-client.png" alt="">
            </span>
        </a>
    </div>
</div>
@endif -->
</div>

@endsection
