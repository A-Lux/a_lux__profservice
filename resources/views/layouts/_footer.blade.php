<div class="contacts" id="contact">
    <div class="row w-100">
        <div class="col-xl-6 col-lg-6">
            <div class="map">
                <iframe src="https://yandex.ru/map-widget/v1/-/CKEIU4nV" width="100%" height="100%" frameborder="1" allowfullscreen="true"></iframe>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="contacts-content">
                <div class="title">
                    <h1>@lang('messages.Свяжитесь с нами')</h1>
                </div>
                <form>
                    <label class="labels_form" for="">@lang('messages.Ваше имя')</label>
                    <input type="text" placeholder="@lang('messages.Ваше имя')" name="name" required>
                    <br>
                    <label class="labels_form" for="">@lang('messages.Email')</label>
                    <input type="text"  name="email" placeholder="@lang('messages.Email')" required>
                    <br>
                    <label class="labels_form" for="">@lang('messages.Комментарии')</label>
                    <textarea name="comment" id="" cols="30" rows="10" placeholder="@lang('messages.Ваше сообщение')"></textarea>
                    <button type="button" class="btn btn-danger btn-service mt-4" id="requestButton">@lang('messages.Отправить')</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- CONTACTS-END -->

<footer>
    <div class="container">
        <div class="footer-link d-flex">
            @foreach (menu('site', '_json') as $menuItem)
            @php $menuItem = $menuItem->translate(app()->getLocale(), 'ru');@endphp
            <a href="{{ $menuItem->url }}">{{ $menuItem->title}}</a>
            @endforeach
        </div>
        <div class="footer-adress">
            <div class="row">
                <div class="col-xl-6">
                    <div class="adress">
                        <p> {{$address->getAddress()}}</p>
                        <div class="silver-text">
                            <p>@lang('messages.Консультационные и практические услуги в области информационных технологий')</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="text-right d-flex justify-content-end">
                        <div class="email">
                            <a href="#">
                                <h4><a href="tel:{{ setting('site.telephone') }}">{{ setting('site.telephone') }}</a></h4>
                            </a>
                            <a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>
                        </div>
                        <div class="social">
                            <a href="{{setting('site.facebook')}}"><i class="fab fa-facebook"></i></a>
                            <a href="{{setting('site.linkedin')}}"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div>

                    <div class="a-lux-link text-right mt-3">
                        <a href="https://www.a-lux.kz/" target="blank">Разработано в <img src="./images/logo_a-lux.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>




<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/ajax.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
@stack('scripts')
</body>

</html>
