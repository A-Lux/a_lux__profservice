@extends('layouts.app')
@section('content')

    <div class="services">
        <div class="services-discription text-center">
            <div class="container">
                <div class="title">
                    <h1>{{ $model->title }}</h1>
                </div>
                <p>{!! $model->content !!}</p>
            </div>
        </div>


        <div class="services-content">
            <div class="container">
                <div class="row">
                    @foreach($services as $k => $v)
                    <div class="col-xl-6 col-md-6 col-12" data-aos="zoom-in">
                            <div class="services-card">
                                <div class="services-card-header d-flex align-items-start">
                                    <img src="{{ asset('storage/'.$v->inner_icon)}}" alt="">
                                    <div class="services-header-text">
                                        <h5>{{$v->name}}</h5>
                                    </div>
                                </div>
                                <div class="services-card-text">
                                    <p>{{$v->content}} </p>
                                </div>
                                <button type="button" class="btn btn-outline-danger btn-outline btn-order"
                                        data-toggle="modal" data-target="#requestServiceModal" onclick="saveServiceID({{$v->id}})">@lang('messages.Заказать')</button>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="services-btn text-center">
                    <a  class="btn btn-outline-danger btn-service btn-anchor" href="#contact">@lang('messages.не нашли нужной услуги? напишите нам!')</a>
                </div>
            </div>
        </div>
    </div>

@endsection
