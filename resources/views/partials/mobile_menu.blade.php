<div class="mobile-menu">
    <div class="logo">
        <a href="/"><img src="./images/logo.png" alt=""></a>
    </div>
    <div class="lang d-flex">
        <a href="/lang/ru" class="{{ app()->isLocale("ru") ? ' lang-active' : '' }}">ru</a>
        <a href="/lang/kz" class="{{ app()->isLocale("kz") ? ' lang-active' : '' }}">kz</a>
        <a href="/lang/en" class="{{ app()->isLocale("en") ? ' lang-active' : '' }}">en</a>
    </div>
    <div class="burger-menu">
        <a href="#" class="burger-menu-btn">
            <span class="burger-menu-lines"></span>
        </a>
    </div>
    <div class="mobile-nav">
        <ul class="mb-0 p-0">
            <li>
                @foreach (menu('site', '_json') as $menuItem)
                    @php $menuItem = $menuItem->translate(app()->getLocale(), 'ru');@endphp
                    <a href="{{ $menuItem->url }}">{{ $menuItem->title}}</a>
                @endforeach
            </li>
        </ul>
    </div>
</div>
