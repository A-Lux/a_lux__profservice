<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Brand extends Model
{

    public static function getAll(){
        return Brand::orderBy('sort', 'ASC')->get();
    }
    
}
