<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.01.2020
 * Time: 9:41
 */

namespace App\Mail;


use App\RequestForProject;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var RequestForProject
     */
    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestForProject $request)
    {
        $this->model = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.project')->subject("Пользователь хочет заказать подобный проект");
    }
}
