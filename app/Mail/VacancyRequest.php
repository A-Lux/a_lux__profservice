<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.01.2020
 * Time: 17:05
 */

namespace App\Mail;


use App\RequestForVacancy;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VacancyRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var RequestForVacancy
     */
    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestForVacancy $model)
    {
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.vacancy')->subject("Пользователь оставил заявку на вакансию");
    }
}
