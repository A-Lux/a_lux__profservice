<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class MainAboutHistory extends Model
{
    use Translatable;
    protected $translatable = ['content'];

    public static function getAll(){
        return MainAboutHistory::get();
    }
}
