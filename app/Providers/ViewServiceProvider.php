<?php

namespace App\Providers;

use App\Address;
use App\Contact;
use App\Helpers\TranslatesCollection;
use App\Page\Page;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Page $page, Address $address)
    {
        View::composer('*', function($view) use($page) {
            $view->with(['page' => $page]);
        });


        View::composer('*', function($view) use($address) {
            $view->with(['address' => $address->first()]);
        });
    }
}
