<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Address extends Model
{
    use Translatable;
    protected $translatable = ['address'];

    public function getAddress(){
        $model = $this->translate(app()->getLocale(), 'ru');
        return $model->address;
    }
}
