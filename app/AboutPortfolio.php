<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutPortfolio extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content'];

    public static function getContent(){
        return AboutPortfolio::find(1);
    }
}
