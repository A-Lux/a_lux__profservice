<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class MainAbout extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content'];

    public static function getContent(){
        return MainAbout::find(1);
    }
}
