<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RequestForVacancy extends Model
{
    protected $fillable = ['name', 'vacancy_id', 'telephone'];

    public function vacancy(){
        return $this->belongsTo(Vacancy::class);
    }
}
