<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Service extends Model
{
    use Translatable;
    protected $translatable = ['name', 'content'];

    public static function getMain(){
        return Service::where('isMain', 1)->orderBy('sort', 'ASC')->get();
    }

    public static function getAll(){
        return Service::orderBy('sort', 'ASC')->get();
    }

}
