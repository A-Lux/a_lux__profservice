<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Project extends Model
{
    use Translatable;
    protected $translatable = ['name', 'content'];

    public static function getAll(){
        return Project::orderBy('sort', 'ASC')->with('types')->get();
    }

    public function types() {
        return $this->hasMany(ProjectType::class);
    }
    
}
