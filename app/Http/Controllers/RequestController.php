<?php

namespace App\Http\Controllers;

use App\Mail\ClientRequest;
use App\Mail\ProjectRequest;
use App\Mail\ServiceRequest;
use App\Mail\VacancyRequest;
use App\RequestForProject;
use App\RequestForService;
use App\RequestForVacancy;
use App\RequestOurCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Request as EmailRequest;
use App\Mail\SiteRequest as SiteRequest;

class RequestController extends Controller
{
    public function site(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = EmailRequest::create($request->all());
            Mail::to(setting('site.email'))->send(new SiteRequest($model));
            return response(['status' => 1,'message' => __('messages.Ваша заявка принята')], 200);
        }
    }


    public function client(Request $request) {
        $validator = Validator::make($request->all(), [
            'fio' => 'required',
            'company' => 'required',
            'face' => 'required',
            'telephone'    => 'required|regex:/(\+77)[0-9]{9}/',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestOurCustomer::create($request->all());
            Mail::to(setting('site.email'))->send(new ClientRequest($model));
            return response(['status' => 1,'message' => 'Ваша заявка принята'], 200);
        }
    }

    public function service(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'telephone'    => 'required|regex:/(\+77)[0-9]{9}/',
            'service_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForService::create($request->all());
            Mail::to(setting('site.email'))->send(new ServiceRequest($model));
            return response(['status' => 1,'message' => 'Ваша заявка принята'], 200);
        }
    }


    public function vacancy(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'vacancy_id' => 'required',
            'telephone'    => 'required|regex:/(\+77)[0-9]{9}/',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForVacancy::create($request->all());
            Mail::to(setting('site.email'))->send(new VacancyRequest($model));
            return response(['status' => 1,'message' => 'Ваша заявка принята'], 200);
        }
    }



    public function project(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'project_id' => 'required',
            'telephone'    => 'required|regex:/(\+77)[0-9]{9}/',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForProject::create($request->all());
            Mail::to(setting('site.email'))->send(new ProjectRequest($model));
            return response(['status' => 1,'message' => 'Ваша заявка принята'], 200);
        }
    }
}
