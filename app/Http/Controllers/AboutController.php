<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.01.2020
 * Time: 9:54
 */

namespace App\Http\Controllers;


use App\AboutMainDirection;
use App\AboutPortfolio;
use App\AboutPortfolioBrand;
use App\AboutService;
use App\AboutStaff;
use App\AboutStaffType;
use App\Helpers\TranslatesCollection;
use App\Service;
use App\TitlePage;
use App\Vacancy;

class AboutController extends Controller
{
    public function Index(){

        $model = TitlePage::getAboutContent();
        $staff = AboutStaff::getContent();
        $staffTypes = AboutStaffType::getAll();
        $portfolio = AboutPortfolio::getContent();
        $portfolioBrands = AboutPortfolioBrand::getAll();
        $mainDirection = AboutMainDirection::getContent();
        $services = AboutService::getAll();
        $vacancies = Vacancy::getAll();

        TranslatesCollection::translate($model, app()->getLocale());
        TranslatesCollection::translate($staff, app()->getLocale());
        TranslatesCollection::translate($staffTypes, app()->getLocale());
        TranslatesCollection::translate($portfolio, app()->getLocale());
        TranslatesCollection::translate($mainDirection, app()->getLocale());
        TranslatesCollection::translate($services, app()->getLocale());
        TranslatesCollection::translate($vacancies, app()->getLocale());


        return view('about.index', compact('model', 'staff','staffTypes','portfolio',
            'portfolioBrands','mainDirection','services','vacancies'));
    }
}
