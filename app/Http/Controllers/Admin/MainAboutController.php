<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.01.2020
 * Time: 14:46
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class MainAboutController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
