<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.01.2020
 * Time: 9:32
 */

namespace App\Http\Controllers;
// use

use App\Banner;
use App\Helpers\TranslatesCollection;
use App\MainAbout;
use App\MainAboutHistory;
use App\MainProject;
use App\OurClient;
use App\Page;
use App\Project;
use App\Service;



class HomeController extends Controller
{
    public function index(){

        $banners = Banner::getAll();
        $about = MainAbout::getContent();
        $histories = MainAboutHistory::getAll();
        $services = Service::getMain();
        $main_project = MainProject::getAll();
        $projects = Project::getAll();
        $clients = OurClient::getAll();

        TranslatesCollection::translate($banners, app()->getLocale());
        TranslatesCollection::translate($about,  app()->getLocale());
        TranslatesCollection::translate($histories, app()->getLocale());
        TranslatesCollection::translate($services, app()->getLocale());
        TranslatesCollection::translate($main_project, app()->getLocale());
        TranslatesCollection::translate($projects, app()->getLocale());
        TranslatesCollection::translate($clients, app()->getLocale());

        return view('home.index', compact('banners','about','histories','services','main_project','clients','projects'));
    }


}
