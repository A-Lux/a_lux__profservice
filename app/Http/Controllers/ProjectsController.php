<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.01.2020
 * Time: 10:07
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\Project;
use App\TitlePage;

class ProjectsController extends Controller
{
    public function Index(){

        $model = TitlePage::getProjectContent();
        $projects = Project::getAll();

        TranslatesCollection::translate($model, app()->getLocale());
        TranslatesCollection::translate($projects, app()->getLocale());
        return view('projects.index', compact( 'model', 'projects'));
    }
}
