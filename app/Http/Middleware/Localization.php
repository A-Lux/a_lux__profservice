<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.01.2020
 * Time: 15:11
 */

namespace App\Http\Middleware;

use Closure;


class Localization
{
    public function handle($request, Closure $next, $guard = null)
    {
        app()->setLocale(session()->get('lang'), 'ru');
        return $next($request);
    }

}
