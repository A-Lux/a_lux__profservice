<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Vacancy extends Model
{
    use Translatable;
    protected $translatable = ['name', 'patch','content','requirements','social_package'];

    public static function getAll(){
        return Vacancy::get();
    }
}
