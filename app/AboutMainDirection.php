<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutMainDirection extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content'];

    public static function getContent(){
        return AboutMainDirection::find(1);
    }
    
}
